import numpy as np
import math

advertisers = { 'A': [0.1, (0.015, 0.01, 0.005),1.0,0],
                'B': [0.09, (0.016, 0.012, 0.006), 2.0, 0 ],
                'C': [0.08, (0.017, 0.014, 0.007),3.0,0],
                'D': [0.07, (0.018, 0.015, 0.008), 4.0, 0],
                'E': [0.06, (0.019,0.016, 0.01),5.0, 0]
                }

result = {}

bidsOfAdvertisers = {key: advertisers[key][0] for key in advertisers}
crtsOfAdvertisers = {key: advertisers[key][1] for key in advertisers}
budgetsOfAdvertisers = {key:advertisers[key][2] for key in advertisers}
numberOfClickThrough = {key:advertisers[key][3] for key in advertisers}
meritsOfAdvertisers = {key: np.array(advertisers[key][1])*advertisers[key][0] for key in advertisers}

print "budget",budgetsOfAdvertisers
print "numberOfClicks",numberOfClickThrough

maxOfAllclicks = 101
while True:
    lastNumOfClicks = sum(numberOfClickThrough.values())
    if lastNumOfClicks >= maxOfAllclicks or len(meritsOfAdvertisers)<3:
        break

    listOfMax = []
    position_to_advertiser = {}

    for i in range(3):
        max = -1
        saveOfKey = None
        for key in meritsOfAdvertisers:
            if meritsOfAdvertisers[key][i] > max and key not in listOfMax:
                max = meritsOfAdvertisers[key][i]
                saveOfKey = key
        if saveOfKey !=None:
            position_to_advertiser[i] = saveOfKey
            listOfMax.append(saveOfKey)


    advertiser_to_leftOfBudget = {position_to_advertiser[p]:budgetsOfAdvertisers[position_to_advertiser[p]] for p in position_to_advertiser.keys()}
    meritsOfAdvertisers_postion = {position_to_advertiser[p]: meritsOfAdvertisers[position_to_advertiser[p]][p] for p in position_to_advertiser}
    rateOfAdvertisers_postion = {position_to_advertiser[p]: crtsOfAdvertisers[position_to_advertiser[p]][p] for p in position_to_advertiser}
    numberOfevent_postion =  {position_to_advertiser[p]: math.floor(budgetsOfAdvertisers[position_to_advertiser[p]]/bidsOfAdvertisers[position_to_advertiser[p]])  for p in position_to_advertiser}
    advertiser_run_out_of_budget = min(numberOfevent_postion.iterkeys(), key=lambda k: numberOfevent_postion[k])

    limitedNumber = math.ceil((maxOfAllclicks - lastNumOfClicks) / sum([ rateOfAdvertisers_postion[position_to_advertiser[p]] / rateOfAdvertisers_postion[
            advertiser_run_out_of_budget] for p in position_to_advertiser]))

    clicksOfThisPhase = min([numberOfevent_postion[advertiser_run_out_of_budget],limitedNumber])

    resultOfEachPhase =  {position_to_advertiser[p]: math.floor(clicksOfThisPhase * rateOfAdvertisers_postion[position_to_advertiser[p]] /rateOfAdvertisers_postion[advertiser_run_out_of_budget]) for p in position_to_advertiser}
    print "checkingPoint",resultOfEachPhase


    meritsOfAdvertisers.pop(advertiser_run_out_of_budget)

    result[advertiser_run_out_of_budget]=resultOfEachPhase[advertiser_run_out_of_budget]

    for a in resultOfEachPhase:
        budgetsOfAdvertisers[a]-=bidsOfAdvertisers[a]*resultOfEachPhase[a]
        numberOfClickThrough[a]+= resultOfEachPhase[a]

    print "budget", budgetsOfAdvertisers
    print "numberOfClicks", numberOfClickThrough



print sum(result.values()),result
print sum(numberOfClickThrough.values()), numberOfClickThrough
print budgetsOfAdvertisers

