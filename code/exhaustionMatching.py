
import itertools

graph = {'a0':['b0','b1'],
         'a1':['b2','b3'],
         'a2':['b0','b4'],
         'a3':['b1','b2'],
         'a4':['b3','b4'],
         'b0':['a0','a1'],
         'b1':['a0','a3'],
         'b2':['a1','a3'],
         'b3':['a1','a4'],
         'b4':['a2','a4']
         }


Alist = ['a0','a1','a2','a3','a4']
Blist = ['b0','b1','b2','b3','b4']


graphSets = []

for node in Alist:
    for point in graph[node]:
        graphSets.append((node,point))

graphSets = set(graphSets)  #O(1) in searching

def isValidmatch(checkList):
    checkList  = reduce(lambda x,y: list(x)+ list(y), checkList)
    return False if len(checkList) > len(set(checkList)) else True

for k in range(1,len(Alist)+1, 1):
    for aMatch in list(itertools.combinations(graphSets,k)):
        if isValidmatch(aMatch):
            print aMatch

