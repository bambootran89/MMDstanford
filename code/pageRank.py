import numpy as np

def pageRank(graph,loops,beta,v,teleport = None):
    vertices = sorted(graph.keys())
    M = []
    for index in range(len(vertices)):
        edges = graph[vertices[index]]
        M.append([1.0/len(edges) if vertices[i] in graph[vertices[index]] else 0 for i in range(len(vertices)) ])
    M = np.matrix(M).transpose()
    print "matrix" , M
    if teleport == None:
        e = np.matrix([1.0 for i in range(len(vertices))]).transpose()
    else:
        e = np.matrix([1.0 if vertices[i] in teleport else 0 for i in range(len(vertices))]).transpose()
    for i in range(loops):
        v = beta*M *v + (1-beta) * e / sum(e)
        print i+1, v

graph = {'A':['B','C'],
             'B':['C','D'],
             'C':['D'],
             'D':['A']}


pageRank(graph,1,0.5,np.matrix([1.0,1.0,1.0,1.0]).transpose(), ['A','B','C','D'])