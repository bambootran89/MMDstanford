import numpy as np

from numpy import linalg as LA
p1 = np.array([1,4])
p2 = np.array([3,6])

def findPerpendicularBisector(p1,p2):
    '''y = ax +b'''
    midpoint = (p1 + p2) / 2.0
    slope = (p2[-1] - p1[-1]) / (p2[0] - p1[0])
    slope = -1.0 / slope
    return np.array([slope, -slope * midpoint[0] + midpoint[-1]])

def findLine(p1,p2):
    return LA.solve([[p1[0], 1], [p2[0], 1]], [p1[-1], p2[-1]])


def findInterpoint(line1, line2):
    b = np.array([line1[-1], line2[-1]])
    a = np.array([[-line1[0],1],[-line2[0],1]])
    return LA.solve(a, b)

print findPerpendicularBisector(p1,p2)
print findLine(p1,p2)
print findInterpoint(findPerpendicularBisector(p1,p2),findLine(p1,p2) )
