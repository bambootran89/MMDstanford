import numpy as np

def findPrimesinRange(num):
    primes = []
    for i in range(2,num+1,1):
        isPrime = True
        for j in range(2, i, 1):

            if i%j==0:
                isPrime = False
                break
        if isPrime:
            primes.append(i)

    return primes




def findPrimesDivisorOfNumber (number, primes):

    result = []
    for i in primes:
        if number%i == 0:
            result.append(i)
    return result

def mapAndreduce(aList):
    result = {}
    for i in aList:
        if i[0] not in result:
             result[i[0]] = i[1]
        else:
            result[i[0]]+=i[1]
    return result


numbers = [15, 21, 24, 30, 49]

primes = findPrimesinRange(max(numbers))


flatmap = reduce(lambda x,y: x+ y, map(lambda n: [(i,n) for i in findPrimesDivisorOfNumber(n, primes)], numbers))

print mapAndreduce(flatmap)
