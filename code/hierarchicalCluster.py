import itertools
import numpy as np

class BaseObject(object):
   def __init__(self, val):
       self.val = val

   def __cmp__(self, other):
        if (self.val == other.val and self.val == "MAX") or (self.val == other.val and self.val == "MIN"):
            return 0
        elif self.val == "MIN" or other.val == "MAX":
            return -1
        elif self.val == "MAX" or other.val == "MIN" :
            return 1
        else:
            return cmp(self.distance(self.val[0], self.val[1]), self.distance(other.val[0], other.val[1]))
   def distance(self,x,y):
       pass
   def valueOfdistance(self, ):
       self.distance(self.val[0], self.val[1])

   def __repr__(self):
       return str(self.val)

   def __hash__(self):
       return hash(str(self.val))
   def __eq__(self, other):
       return str(self.val) == str(other.val)

class ObjectL1(BaseObject):
    def distance(self,x,y):
        return self.normL1distance(x,y)

    def normL1distance(self, x, y):
        return sum(abs(np.array(list(x)) - np.array(list(y))))

class ObjectNonEuclidean(BaseObject):
    def __init__(self,val, globalObject):
        BaseObject.__init__(self,val)
        self.globalObject = globalObject
    def distance(self, x, y):
        return self.jaccardDistance(self.globalObject[x],self.globalObject[y])
    def jaccardDistance(self,x,y):
        return 1 - 1.0 * sum(x * y) / sum(x | y)

class ObjectL2(BaseObject):
    def distance(self,x,y):
        return self.normL2Distance(x,y)
    def normL2Distance(self, x, y):
        return np.linalg.norm(np.array(list(x)) - np.array(list(y)))


class PriorityQueue:
    def __init__(self,size,minObject):
        self._queue = [None for i in range(size)]
        self.current = -1
        self.size = size
        self.value_to_index = {} # it's very useful for search/delete if we don't push similar item
        self.minObject = minObject
    def parent(self,i):
        return int((i-1)/2.0)
    def leftChild(self,i):
        return 2*i +1
    def rightChild(self,i):
        return 2*i + 2
    def put(self,item):
        if self.current==self.size-1:
            print "meet the limit size"
            return
        self.current+=1
        self._queue[self.current] = item
        self.value_to_index[item] = self.current
        i = self.current
        while i!=0 and self._queue[self.parent(i)] > self._queue[i]:
            self.swap(i,self.parent(i))
            i = self.parent(i)

    def swap(self,x,y):
        self.value_to_index[self._queue[x]] = y
        self.value_to_index[self._queue[y]] = x
        self._queue[x],self._queue[y] = self._queue[y],self._queue[x]

    def minHeapify(sefl,i):

        l = sefl.leftChild(i)
        r = sefl.rightChild(i)
        mmin = i
        if (l <= sefl.current and sefl._queue[l] < sefl._queue[mmin]):
            mmin = l;
        if (r <= sefl.current and sefl._queue[r] < sefl._queue[mmin]):
            mmin = r;
        if i != mmin:
            sefl.swap(i,mmin)
            sefl.minHeapify(mmin)

    def pop(self):
        if self.current == -1:
            print "empty list"
            return None
        object = self._queue[0]
        self.value_to_index.pop(self._queue[0])

        self._queue[0] = self._queue[self.current]
        self.value_to_index[self._queue[0]] = 0
        self._queue[self.current] = None
        self.current-=1
        self.minHeapify(0)
        return object
    def deleteItem(self,item):
        i = self.value_to_index.get(item)
        if i == None:
            return
        if (i < 0 or i > self.current):
            return;

        self.decreaseKey(i, self.minObject);
        self.pop()

    def decreaseKey(sefl, i, new_val):
        sefl.value_to_index.pop(sefl._queue[i])
        sefl._queue[i] = new_val
        sefl.value_to_index[new_val] = i

        while (i != 0 and sefl._queue[sefl.parent(i)] > sefl._queue[i]):
            sefl.swap(i,sefl.parent(i))
            i = sefl.parent(i)

    def __repr__(self):
        return str(self._queue)



def hierarchicalClustering(points,TypeOfPoint,callback,pref=None):
    if pref==None:
        pairs = [ TypeOfPoint(v) for v in list(itertools.combinations(points,2)) ]
    else:
        pairs = [ TypeOfPoint(v,pref) for v in list(itertools.combinations(points,2)) ]
    clusters = {points[id]:[points[id]] for id in range(len(points))}

    # create priority quite
    minObject = TypeOfPoint("MIN") if pref==None else TypeOfPoint("MIN",pref)
    q = PriorityQueue(len(pairs), minObject)

    for pair in pairs:
        q.put(pair)

    # merging 2 clusters
    loop = 0
    while True:
        if len(clusters.keys()) <=1:
            break
        selected = q.pop()

        for k in selected.val:
            for p in clusters:
                if p == k:
                    continue
                if pref==None:
                    q.deleteItem(TypeOfPoint((p,k)))
                    q.deleteItem(TypeOfPoint((k,p)))
                else:
                    q.deleteItem(TypeOfPoint((p, k),pref))
                    q.deleteItem(TypeOfPoint((k, p),pref))

        cluster_1 = clusters.pop(selected.val[0])
        cluster_2 = clusters.pop(selected.val[1])

        newCentroid = callback(cluster_1,cluster_2)

        clusters[newCentroid] = cluster_1 + cluster_2
        # re-caluculate pairs of new centroid and other centroid
        for centroid in clusters:
            if centroid ==  newCentroid:
                continue
            if pref==None:
                q.put(TypeOfPoint((newCentroid, centroid)))
            else:
                q.put(TypeOfPoint((newCentroid, centroid),pref ))
        print "layer ",loop+1
        print clusters
        loop+=1
print "Example of euclidean case"
points = [ tuple([i*i]) for i in range(1, 9,1) ]
    # create all pairs/distances respectively
hierarchicalClustering(points,ObjectL1,lambda cluster_1, cluster_2: tuple(i for i in np.mean(cluster_1+cluster_2, axis=0)),pref=None)

print "Example of non euclidean case"
point_name = {'a': np.array([1, 0, 0]),
              'c': np.array([0, 1, 0]),
              'b': np.array([1, 1, 0]),
              'e': np.array([0, 0, 0]),
              'd': np.array([1, 1, 1]),
              'g': np.array([1, 0, 1]),
              'f': np.array([0, 0, 1]),
              'h': np.array([0, 0, 1])
              }
points = point_name.keys()

def findclustroid(cluster_1, cluster_2):
    points = cluster_1 + cluster_2
    def jaccard_distance(x,y):
        return 1 - 1.0 * sum(x * y) / sum(x | y)
    point_sumOfdistance = {}
    for p in points:
        sumOfdistance = 0
        for other in points:
            if p!=other:
                sumOfdistance+=jaccard_distance(point_name[p], point_name[other])
        point_sumOfdistance[p] = sumOfdistance
    return min(point_sumOfdistance, key= point_sumOfdistance.get)

hierarchicalClustering(points,ObjectNonEuclidean,findclustroid, pref=point_name)

