import Queue as Q

class Dist(object):
    def __init__(self, dis, nodeA, nodeB):
        self.dis = dis
        self.nodeA = nodeA
        self.nodeB = nodeB
    def __cmp__(self, other):
        return self.dis - other.dis

    def __repr__(self):
        return ("(%s,%s)") % (str(self.nodeA), str(self.nodeB))


edges = [Dist(5, 'A', 'B'), Dist(10, 'C', 'D'), Dist(20, 'A', 'E'), Dist(30, 'F', 'G')]
edges.sort()
dict_done = {}
for i in range(len(edges)):
    next_level = edges[i]
    if dict_done.get(next_level.nodeA,False) == False and dict_done.get(next_level.nodeB,False) == False:
        dict_done[next_level.nodeA] = True;
        dict_done[next_level.nodeB] = True;
        print ('NEXT: ', next_level)



