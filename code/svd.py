import numpy as np

#matrix U
#matrix V

m = np.array([np.array([5,2,4,4,3]),
              np.array([3,1,2,4,1]),
              np.array([2,None,3,1,4]),
              np.array([2,5,4,3,5]),
              np.array([4,4,5,4,None])
              ])

none_position = {}
# in the real case, this step will be included in create m so dont worry the BigO

for i in range(len(m)):
    for j in range(len(m[i])):
        if m[i][j] == None:
            none_position[i] = j
print none_position

u = np.full((5,2),1.0)
v = np.full((2,5),1.0)

def findUrsAndSet(r,s,u,v,m):
    mrj = m[r]
    vsj = v[s]
    urk = u[r]
    k = np.full(len(urk), 1.0)
    k[s] = 0
    none_index_in_m = np.array([1 if i!=None else 0 for i in mrj])
    mrj = np.array([i if i!=None else 0 for i in mrj])
    vsj= vsj*none_index_in_m
    uvrj = np.array([(urk*k).dot(v[:,j]) for j in range(len(vsj))])
    x = vsj.dot(mrj - uvrj)/vsj.dot(vsj)
    u[r][s] = x

def findVrsAndSet(r,s,u,v,m):
    uir = u[:,r]
    mis = m[:,s]
    vks = v[:,s]
    k = np.full(len(vks), 1.0)
    k[r] = 0
    none_index_in_m = np.array([1 if i != None else 0 for i in mis])
    mis = np.array([i if i != None else 0 for i in mis])
    uir = uir * none_index_in_m
    uvis = np.array([(u[j]).dot(vks*k) for j in range(len(uir))])
    y = uir.dot(mis- uvis)/uir.dot(uir)
    v[r][s] = y

square_error_list = []
for i in range(40):
    for r in range(np.shape(u)[0]):
        for s in range(np.shape(u)[1]):
            findUrsAndSet(r, s, u, v, m)
    for r in range(np.shape(v)[0]):
        for s in range(np.shape(v)[1]):
            findVrsAndSet(r, s, u, v, m)

    p = u.dot(v)

    error = 0
    for i in range(len(p)):
        y = none_position.get(i, None)
        if y != None:
            mi = np.array([m[i][k] if k != y else 0 for k in range(len(m[i]))])
            none_index = np.array([1 if k != y else 0 for k in range(len(m[i]))])
            error += (p[i]* none_index - mi).dot(p[i]* none_index - mi)
        else:
            error += (p[i] - m[i]).dot(p[i] - m[i])
    if len(square_error_list) > 0 and abs(square_error_list[-1] - error) < 0.1:
        break
    square_error_list.append(error)

import matplotlib.pyplot as plt
print square_error_list
print u.dot(v)
plt.plot(range(1,len(square_error_list)+1,1),square_error_list,linestyle = "-",color='r')
plt.ylabel("square error",size = 25)
plt.xlabel("iteration",size = 25)
plt.show()



