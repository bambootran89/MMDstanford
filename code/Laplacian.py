import numpy as np

graph = {1: (2,8),
         2: (1,3,8),
         3: (2,4,7),
         4: (3,5,6),
         5: (4,6),
         6: (4,5,7),
         7: (3,6,8),
         8: (1,2,7),
        }


def createAdjacency(graph):
    matrix = []
    maxOfNode =  max(graph.keys())
    for node in graph:
        matrix.append([1 if i in graph[node] else 0 for i in range(1,maxOfNode+1,1)])

    return np.matrix(matrix)

Adj = createAdjacency(graph)

def createDegree(graph):
    v = []
    for node in graph:
        v.append(len(graph[node]))

    return np.diag(v)


Degree = createDegree(graph)
Laplacian = Degree - Adj


print Laplacian
