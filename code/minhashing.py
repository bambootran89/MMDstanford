import numpy as np

def minhashing():
    import itertools
    pers = list(itertools.permutations([1,2,3,4,5,6]))
    rows = {1:0, 2:1, 3:1, 4:0, 5:0, 6:0}

    minhash_count = {key:0 for key in rows.keys()}
    for p in pers:
        for index in range(len(p)+1):
            if rows[p[index]] == 1:
                minhash_count[index+1] = minhash_count[index+1]+1
                break
    print len(pers)
    print minhash_count

minhashing()