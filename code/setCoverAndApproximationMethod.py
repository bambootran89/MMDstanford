import numpy as np

colections = ["AB", "BC", "CD", "DE", "EF", "FG", "GH", "AH", "ADG", "ADF"]

def itemsets(collects):
    itemsets = []
    for itemset in collects:
        itemsets += list(itemset)
    return tuple(set(itemsets))


sortedColections = sorted(colections,cmp=lambda x,y: 1 if len(x)>len(y) else 0 if len(x) == len(y) else -1, reverse=True)

import itertools

def setCover(colects):
    #stupid algorithm
    elements = sorted(itemsets(colects))
    for k in range(1,len(colections)+1,1):
        for aset in list(itertools.combinations(colects,k)):
            gotit = sorted(itemsets(aset))
            if gotit==elements:
                return (len(aset),aset, gotit)

optimal = setCover(colections)
#dump algorithm
def dumpAlgorithm(colects):
    sets = []
    elements = sorted(itemsets(colections))
    for item in colections:
        sets.append(item)
        gotit = sorted(itemsets(sets))
        if gotit==elements:
            return (len(sets), sets, gotit)

dumpResult =  dumpAlgorithm(colections)
print dumpResult
print "dump = ",dumpResult[0]*1.0/optimal[0]

def simpleAlgorithm(colects):
    elements = {i:1 for i in itemsets(colections)}
    current = {}
    result = []

    for item in colects:
        elementsOfItem = list(item)
        for element in elementsOfItem:
            if current.get(element,None)==None:
                #select this item
                for e in elementsOfItem:
                    elements.pop(e,None)
                    current[e] = 1

                result.append(item)
                break
        if len(elements) == 0:
            return (len(result),result)


simpleResult =  simpleAlgorithm(colections)
print simpleResult
print "simple = ",simpleResult[0]*1.0/optimal[0]

largestFirst = simpleAlgorithm(sortedColections)
print largestFirst
print "largestFirst = ",largestFirst[0]*1.0/optimal[0]

def mostHelp(colects):
    elements = {i: 1 for i in itemsets(colections)}
    current = {}
    result = []
    dictOfColects = {item:1 for item in colects}
    while True:

        maxOfUncoveredElement = 0
        saveOfMax = None
        for k in dictOfColects:
            countOfUncoveredElement = 0

            for e in list(k):
                if current.get(e)==None:
                    countOfUncoveredElement+=1
            if maxOfUncoveredElement<countOfUncoveredElement:
                maxOfUncoveredElement=countOfUncoveredElement
                saveOfMax = k
        dictOfColects.pop(saveOfMax,None)
        result.append(saveOfMax)
        for e in list(saveOfMax):
            elements.pop(e,None)
            current[e] = 1
        if len(elements)== 0:
            return (len(result), result)


mostHelpResult = mostHelp(colections)
print mostHelpResult
print "mostHelp = ",mostHelpResult[0]*1.0/optimal[0]

