import numpy as np
umatrix = {'a':[4,0,2],
           'b':[5,3,0],
           'c':[0,4,1],
           'd':[5,3,3],
           'e':[1,1,0],
           'f':[0,2,4],
           'g':[3,1,5],
           'h':[2,0,3]
            }


def jaccard(x,y):
    return 1- 1.0*sum(x*y)/sum(x|y)

def normalize_umtrix():
    for key in umatrix:
        umatrix[key] = np.array([1 if x >=3 else 0 for x in umatrix[key] ])

normalize_umtrix()
print umatrix
keys = sorted(umatrix.keys())
keys = ['a','c','d', 'e', 'f']

#keys = ['a','b','d']
pairs = []
for k1 in range(len(keys)):
    for k2 in range(k1+1, len(keys),1):
        x = umatrix[keys[k1]]
        y = umatrix[keys[k2]]
        pairs.append((keys[k1], keys[k2], jaccard(x,y)))
print sorted(pairs,key=lambda x:x[2])


