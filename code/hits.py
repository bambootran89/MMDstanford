import numpy as np


graph = {'A':['B','C'],
             'B':['C','D'],
             'C':['D'],
             'D':['A']}


def hits(graph,loops,h):
    vertices = sorted(graph.keys())
    L = []
    for index in range(len(vertices)):
        L.append([1.0 if vertices[i] in graph[vertices[index]] else 0 for i in range(len(vertices)) ])
    L = np.matrix(L)
    print " L = ",L
    LT = L.transpose()

    for i in range(loops):
        a = LT.dot(h)
        #a = 1.0 * a / max(a)
        h = L.dot(a)
        #h = 1.0 * h / max(h)
        print i+1,"authority", a

        print i+1, "happyness", h

hits(graph,1,np.matrix([1, 1, 1, 1]).transpose()*1.0)

