import numpy as np

import random

def distance(x,y):
    return abs(x-y)

def average(data):
    return np.average(data)

def k_mean(data,knumber):
    centroids = []
    while True:
        centroids.append(random.choice(data))
        if len(set(centroids))== knumber:
            break
    centroids = list(set(centroids))
    centroids = [36,81]
    
    for i in range(20):
        data_distance_to_centroid = {d:{} for d in data}
        centroid_data = {c:[] for c in centroids}
        for d in data:
            for c in centroids:
                data_distance_to_centroid[d][c] = distance(d, c)

        data_distance_to_centroid = {ky: min(data_distance_to_centroid[ky], key=lambda k: data_distance_to_centroid[ky][k]) for ky in data_distance_to_centroid }

        for d in data_distance_to_centroid:
            centroid_data[data_distance_to_centroid[d]].append(d)
        print centroid_data
        print centroids
        centroids = [average(centroid_data[d]) for d in centroids]

data = [i*i for i in range(1,11,1)]

k_mean(data,2)




